// Copyright 2007 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @fileoverview
 * Javascript code for the interactive AJAX shell.
 *
 * Part of http://code.google.com/p/google-app-engine-samples/.
 *
 * Includes a function (shell.runStatement) that sends the current python
 * statement in the shell prompt text box to the server, and a callback
 * (shell.done) that displays the results when the XmlHttpRequest returns.
 *
 * Also includes cross-browser code (shell.getXmlHttpRequest) to get an
 * XmlHttpRequest.
 */

var scriptrunner = {}

/**
 * A constant for the XmlHttpRequest 'done' state.
 * @type Number
 */
scriptrunner.DONE_STATE = 4;

/**
 * A cross-browser function to get an XmlHttpRequest object.
 *
 * @return {XmlHttpRequest?} a new XmlHttpRequest
 */
scriptrunner.getXmlHttpRequest = function() {
  if (window.XMLHttpRequest) {
    return new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    try {
      return new ActiveXObject('Msxml2.XMLHTTP');
    } catch(e) {
      return new ActiveXObject('Microsoft.XMLHTTP');
    }
  }

  return null;
};


/**
 * The XmlHttpRequest callback. If the request succeeds, it adds the command
 * and its resulting output to the shell history div.
 *
 * @param {XmlHttpRequest} req the XmlHttpRequest we used to send the current
 *     statement to the server
 */
scriptrunner.done = function(req) {
  if (req.readyState == this.DONE_STATE) {
      var output = document.getElementById("scriptrunneroutput");
      output.innerHTML = "<pre>" + req.responseText + "</pre>";
      var runbutton = document.getElementById("scriptrunbutton");
      runbutton.innerHTML = "Run script";
      runbutton.disabled = false;
      switchtotab('scriptrunneroutput');
  }
};

/**
 * This is the form's onsubmit handler. It sends the python statement to the
 * server, and registers shell.done() as the callback to run when it returns.
 *
 * @return {Boolean} false to tell the browser not to submit the form.
 */
scriptrunner.sendscript = function() {
  var script = document.getElementById('editor').bespin.editor.value;

  // build a XmlHttpRequest
  var req = this.getXmlHttpRequest();
  if (!req) {
    document.getElementById('scriptrunneroutput').innerHTML =
        "<span class='error'>Your browser doesn't support AJAX. :(</span>";
    return false;
  }

  req.onreadystatechange = function() { scriptrunner.done(req); };

  // send the request and tell the user.
  var runbutton = document.getElementById('scriptrunbutton')
  runbutton.innerHTML = "Running...";
  runbutton.disabled = true;
  req.open("post", "shell.do", true);
  req.setRequestHeader('Content-type','text/x-python;charset=UTF-8');
  req.send(script);

  return false;
};
